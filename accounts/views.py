from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.contrib.auth.models import User

from django.conf import settings
from twilio.rest import Client
from twilio.base.exceptions import TwilioRestException
import re

from .forms import VerifiedPhoneForm, PhoneNumberForm, CheckOtpForm, GetPasswordForm
from .models import TempRegistration


def is_phone_valid(phone_number):
    """Check phone number is valid or not. @return: Boolean """
    if phone_number:
        MOBILE_REGEX = re.compile('^(?:\+?88)?01[15-9]\d{8}$')
        if MOBILE_REGEX.match(phone_number):
            return True
        else:
            return False
    else:
        return False


def generate_otp():
    # TODO: genrate random otp
    return 123456


def get_phone_number(request):
    """Get User's Phone number"""

    if request.method == 'POST':
        forms = PhoneNumberForm(request.POST)

        if forms.is_valid():
            phone_number = forms.cleaned_data['phone_number']

            if is_phone_valid(phone_number):
                try:
                    User.objects.get(username=phone_number)
                    context = {"forms": forms, "errMsg": "User with this phone number already exist"}
                    return render(request, 'accounts/get_phone_number.html', context)
                except:
                    pass
                request.session["phone_number"] = phone_number
                otp = generate_otp()
                TempRegistration.objects.create(phone_number=phone_number, otp_code=otp)
                return redirect('accounts:check-otp')
            else:
                # TODO: push error message
                pass
        else:
            # TODO: push error message to template
            pass

    forms = PhoneNumberForm()
    context = {"forms": forms}
    return render(request, 'accounts/get_phone_number.html', context)


def check_otp(request):
    """Check otp sent to User's Phone"""
    if request.method == 'POST':
        otp_form = CheckOtpForm(request.POST)
        if otp_form.is_valid():
            phone_number = request.session["phone_number"]
            otp_code = otp_form.cleaned_data['otp_code']
            if TempRegistration.objects.filter(phone_number=phone_number, otp_code=otp_code).exists():
                return redirect('accounts:get-password')
    forms = CheckOtpForm()
    context = {"forms": forms}
    return render(request, 'accounts/check_otp.html', context)


def get_password(request):
    """Password and repassword"""
    if request.method == 'POST':
        form = GetPasswordForm(request.POST)

        if form.is_valid():
            password = form.cleaned_data["password"]
            re_password = form.cleaned_data["re_password"]
            phone_number = request.session["phone_number"]

            if password == re_password:
                try:
                    User.objects.create_user(username=phone_number, password=password)
                    return redirect('home')
                except Exception as e:
                    context = {"errmsg": "Failed! Try again"}
                    return render(request, 'accounts/get_password.html', context)
    forms = GetPasswordForm()
    context = {"forms": forms}
    return render(request, 'accounts/get_password.html', context)