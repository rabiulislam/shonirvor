from django.conf.urls import url
from . import views
app_name = 'accounts'
urlpatterns = [
    url(r'^get-phone-number/', views.get_phone_number, name='register'),
    url(r'^check-otp/', views.check_otp, name='check-otp'),
    url(r'^get-password/', views.get_password, name='get-password')
]
