from django import forms


class VerifiedPhoneForm(forms.Form):
    phone_number =  forms.CharField(widget=forms.TextInput(attrs={'class': 'phone_number'}))


class PhoneNumberForm(forms.Form):
    phone_number = forms.CharField(widget=forms.TextInput(attrs={'class': 'phone_number'}))


class CheckOtpForm(forms.Form):
    otp_code = forms.IntegerField()


class GetPasswordForm(forms.Form):
    password = forms.CharField()
    re_password = forms.CharField()
