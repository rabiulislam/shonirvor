from django.db import models


class TempRegistration(models.Model):
    phone_number = models.CharField(max_length=45)
    otp_code = models.CharField(max_length=6)

    def __str__(self):
        return self.phone_number