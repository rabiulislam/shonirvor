from django.shortcuts import render, redirect
from .forms import ServiceForm
from .models import Service, ServiceCategory


def service_category(request):
    form = ServiceCategory.objects.all()
    context = {'form':form}
    return render(request, 'services/service_category.html', context)


def service(request):
    if request.method == 'POST':
        form = ServiceForm(request.POST, request.FILES)
        if form.is_valid():
            value = form.cleaned_data['provider']
            form.save()
            context = {'form':form}
            return redirect('service_details', provider=value)
    else:
        form = ServiceForm()
        context = {'form':form}
        return render(request,'services/service.html', context)


def service_details(request, provider):
    form = Service.objects.get(provider=provider)
    context = {'form':form}
    return render(request,'services/service_details.html', context)


def categorywise_service(request, service_category):
    try:
        form = Service.objects.filter(category__category_name=service_category)
        context = {'form': form}
    except:
        context = {'errmsg':'You have no service'}
    return render(request,'services/categorywise_service.html',context)


def service_edit(request, pk):
    service = Service.objects.get(pk=pk)

    if request.method == 'POST':
        form = ServiceForm(request.POST, request.FILES, instance=service)
        if form.is_valid():
            value = form.cleaned_data['provider']
            form.save()
            return redirect('service_details', provider=value)

    else:
        form = ServiceForm(instance=service)
        return render(request, 'services/service_edit.html',
                      {'form': form})
