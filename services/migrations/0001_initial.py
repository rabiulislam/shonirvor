# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-01-09 09:19
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Service_catagory',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('catagory_name', models.CharField(max_length=200)),
            ],
        ),
        migrations.CreateModel(
            name='Serviece_add',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('service_name', models.CharField(max_length=200)),
                ('provider', models.CharField(max_length=200)),
                ('photo', models.ImageField(blank=True, upload_to='')),
                ('created_date', models.DateTimeField(auto_now_add=True)),
                ('is_active', models.BooleanField()),
                ('catagory', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='services.Service_catagory')),
            ],
        ),
    ]
