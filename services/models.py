from django.db import models
from django.contrib.auth.models import User


class ServiceCategory(models.Model):
    category_name = models.CharField(max_length=200)

    def __str__(self):
        return self.category_name


class Service(models.Model):
    service_name = models.CharField(max_length=200)
    category = models.ForeignKey(ServiceCategory)
    provider = models.ForeignKey(User)
    photo = models.ImageField(upload_to='service_image', blank=True)
    created_date = models.DateTimeField(auto_now_add=True)
    is_active = models.BooleanField()

    def __str__(self):
        return self.service_name

