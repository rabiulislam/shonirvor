from django.conf.urls import url
from .views import service_category, service, service_details, categorywise_service, service_edit

urlpatterns = [
    url(r'^service_category/', service_category, name='service_category'),
    url(r'^service/', service, name='service'),
    url(r'^service_details/(?P<provider>\w+)/', service_details,
        name='service_details'),
    url(r'^service_edit/(?P<pk>\d+)/', service_edit, name='service_edit'),
    url(r'^categorywise_service/(?P<service_category>\w+)/$',
        categorywise_service, name='categorywise_service'),
]
