from django.shortcuts import render

from services.models import Service, ServiceCategory


def home(request):
    services = Service.objects.all()
    context = {'services':services}
    return render(request, 'home.html', context)